//
//  SignUpViewController.swift
//  MotionVibe
//
//  Created by Vadim Rupets on 07.10.16.
//  Copyright © 2016 Vadim Rupets. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var wallpaperImageView: UIImageView?
    @IBOutlet weak var logoImageView: UIImageView?
    @IBOutlet weak var barcodeView: UIView?
    @IBOutlet weak var barcodeTextField: UITextField?
    @IBOutlet weak var lastNameView: UIView?
    @IBOutlet weak var lastNameTextField: UITextField?
    @IBOutlet weak var signUpButton: UIButton?
    @IBOutlet weak var footerView: UIView?
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupWallPaperAndLogo()
        setupBarcodeAndLastNameViews()
        addTapGestureRecognizer()
        setupSignUpButton()
        setupFooter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Setup UI
    
    func setupWallPaperAndLogo() {
        let plist = (UIApplication.shared.delegate as! AppDelegate).plist
        
        let wallPaperImage = UIImage.init(named: plist?.object(forKey: "Wallpaper") as! String)
        self.wallpaperImageView?.image = wallPaperImage
        
        let logoImage = UIImage.init(named: plist?.object(forKey: "Logo") as! String)
        self.logoImageView?.image = logoImage
    }
    
    func setupNavigationBar() {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func setupBarcodeAndLastNameViews() {
        barcodeView?.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        lastNameView?.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        
        barcodeTextField?.attributedPlaceholder = NSAttributedString(string: "Barcode", attributes: [NSForegroundColorAttributeName: UIColor.white])
        lastNameTextField?.attributedPlaceholder = NSAttributedString(string: "Last Name", attributes: [NSForegroundColorAttributeName: UIColor.white])
    }
    
    func setupSignUpButton() {
        signUpButton?.layer.cornerRadius = 10.0
    }
    
    func setupFooter() {
        footerView?.backgroundColor? = UIColor.black.withAlphaComponent(0.65)
    }
    
    //MARK: - Gesture recognizer
    
    func addTapGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture))
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func tapGesture() {
        barcodeTextField?.resignFirstResponder()
        lastNameTextField?.resignFirstResponder()
    }
    
    //MARK: - Button Action
    
    @IBAction func didPressSignUpButton() {
    
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == barcodeTextField {
            lastNameTextField?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return false
    }
}
