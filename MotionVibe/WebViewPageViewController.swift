//
//  WebViewPageViewController.swift
//  MotionVibe
//
//  Created by Vadim Rupets on 06.10.16.
//  Copyright © 2016 Vadim Rupets. All rights reserved.
//

import UIKit

class WebViewPageViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView?
    
    var webViewURLString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let plist = (UIApplication.shared.delegate as! AppDelegate).plist
        
        let webViewURL = URL(string: plist?.object(forKey: "Class Schedule URL") as! String)
        let webViewURLRequest = URLRequest(url: webViewURL!)
        self.webView?.loadRequest(webViewURLRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "CLASS SCHEDULE"
        
        let attrs = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont(name: "Oswald-Regular", size: 20)!
        ]
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.barTintColor = UIColor(red: 39.0 / 255.0, green: 132.0 / 255.0, blue: 174.0 / 255.0, alpha: 1.0)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = attrs
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
