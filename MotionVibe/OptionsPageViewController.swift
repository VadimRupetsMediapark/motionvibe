//
//  OptionsPageViewController.swift
//  MotionVibe
//
//  Created by Vadim Rupets on 06.10.16.
//  Copyright © 2016 Vadim Rupets. All rights reserved.
//

import UIKit

class OptionsPageViewController: UIViewController {
    
    @IBOutlet weak var buildVersionLabel: UILabel?
    @IBOutlet weak var wallpaperImageView: UIImageView?
    @IBOutlet weak var logoImageView: UIImageView?
    @IBOutlet weak var buttonsView: UIView?

    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        
        let plist = (UIApplication.shared.delegate as! AppDelegate).plist
        
        let wallPaperImage = UIImage.init(named: plist?.object(forKey: "Wallpaper") as! String)
        self.wallpaperImageView?.image = wallPaperImage

        let logoImage = UIImage.init(named: plist?.object(forKey: "Logo") as! String)
        self.logoImageView?.image = logoImage
        
        let buildVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        self.buildVersionLabel?.text = "version " + buildVersion!
        
        self.buttonsView?.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        self.buttonsView?.layer.cornerRadius = 2.0
        self.buttonsView?.alpha = 0.0
        
        perform(#selector(self.showButtonsView), with: nil, afterDelay: 1.5)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = true
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showButtonsView() {
        UIView.animate(withDuration: 0.5) { 
            self.buttonsView?.alpha = 1.0
        }
    }
}

