//
//  MemberLoginViewController.swift
//  MotionVibe
//
//  Created by Vadim Rupets on 06.10.16.
//  Copyright © 2016 Vadim Rupets. All rights reserved.
//

import UIKit

class MemberLoginViewController: UIViewController {
    
    @IBOutlet weak var wallpaperImageView: UIImageView?
    @IBOutlet weak var logoImageView: UIImageView?
    @IBOutlet weak var usernameView: UIView?
    @IBOutlet weak var passwordView: UIView?
    @IBOutlet weak var usernameTextField: UITextField?
    @IBOutlet weak var passwordTextField: UITextField?
    @IBOutlet weak var checkboxImageView: UIImageView?
    @IBOutlet weak var letsGoButton: UIButton?
    @IBOutlet weak var footerView: UIView?
    
    var checkboxIsChecked = false
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupWallPaperAndLogo()
        setupTextFields()
        setupLetsGoButton()
        setupFooter()
        addTapGestureRecognizer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setup UI
    
    func setupWallPaperAndLogo() {
        let plist = (UIApplication.shared.delegate as! AppDelegate).plist
        
        let wallPaperImage = UIImage.init(named: plist?.object(forKey: "Wallpaper") as! String)
        wallpaperImageView?.image = wallPaperImage
        
        let logoImage = UIImage.init(named: plist?.object(forKey: "Logo") as! String)
        logoImageView?.image = logoImage
    }
    
    func setupNavigationBar() {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func setupTextFields() {
        usernameView?.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        passwordView?.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        
        usernameTextField?.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSForegroundColorAttributeName: UIColor.white])
        passwordTextField?.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName: UIColor.white])
    }
    
    func setupLetsGoButton() {
        letsGoButton?.layer.cornerRadius = 10.0;
    }
    
    func setupFooter() {
        footerView?.backgroundColor = UIColor.black.withAlphaComponent(0.65)
    }
    
    // MARK: - Gesture recognizer
    
    func addTapGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture))
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func tapGesture() {
        usernameTextField?.resignFirstResponder()
        passwordTextField?.resignFirstResponder()
    }
    
    // MARK: - Button Actions 
    
    @IBAction func didPressRememberMeButton() {
        if (checkboxIsChecked) {
            checkboxImageView?.image = #imageLiteral(resourceName: "Checkbox_no")
        } else {
            checkboxImageView?.image = #imageLiteral(resourceName: "Checkbox_yes")
        }
        
        checkboxIsChecked = !checkboxIsChecked
    }
    
    @IBAction func didPressLetsGoButton() {
        
    }
    
    @IBAction func didPressICantLoginButton() {
        
    }
}

// MARK: - UITextFieldDelegate

extension MemberLoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTextField {
            passwordTextField?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return false
    }
}
