//
//  HomePageViewController.swift
//  MotionVibe
//
//  Created by Vadim Rupets on 07.10.16.
//  Copyright © 2016 Vadim Rupets. All rights reserved.
//

import UIKit

class HomePageViewController: UIViewController {
    
    //MARK: - Carousel views
    
    @IBOutlet weak var avatarImageView: UIImageView? {
        didSet {
            avatarImageView?.layer.cornerRadius = 50
            avatarImageView?.layer.borderColor = UIColor.white.cgColor
            avatarImageView?.layer.borderWidth = 2.0
            avatarImageView?.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var goodMorningLabel: UILabel?
    @IBOutlet weak var goodMorningLabelLeftConstraint: NSLayoutConstraint? {
        didSet {
            goodMorningLabelLeftConstraint?.constant = (UIScreen.main.bounds.size.width - 112 - 70) / 2.0
        }
    }
    
    @IBOutlet weak var checkInCountLabel: UILabel?
    @IBOutlet weak var durationCountLabel: UILabel?
    @IBOutlet weak var pointsCountLabel: UILabel?
    
    @IBOutlet weak var youHaveLabelLeftConstraint: NSLayoutConstraint? {
        didSet {
            if UIScreen.main.bounds.size.height != 568 {
                youHaveLabelLeftConstraint?.constant = 49
            }
        }
    }
    
    //MARK: - Tile Department Navigation
    
    @IBOutlet weak var tileNavigationViewHeightConstraint: NSLayoutConstraint?
    @IBOutlet var tilesCollection: [UIImageView]?
    @IBOutlet var tilesButtonCollection: [UIButton]?
    
    //MARK: - Feedback
    
    @IBOutlet weak var contactMemberServicesButton: UIButton? {
        didSet {
            contactMemberServicesButton?.layer.borderColor = UIColor(red: 39.0 / 255.0, green: 132.0 / 255.0, blue: 174.0 / 255.0, alpha: 1).cgColor
            contactMemberServicesButton?.layer.borderWidth = 5;
            contactMemberServicesButton?.layer.cornerRadius = 25;
        }
    }
    
    //MARK: - Connection Views
    
    @IBOutlet weak var connectionViewHeightConstraint: NSLayoutConstraint?
    @IBOutlet var connectionImageViewCollection: [UIImageView]?
    @IBOutlet var connectionButtonsCollection: [UIButton]?
    @IBOutlet var connectionIconsCollection: [UIImageView]?
    
    //MARK: - Location Views
    
    @IBOutlet var homeClubView: UIView? {
        didSet {
            homeClubView?.layer.borderColor = UIColor.lightGray.cgColor
            homeClubView?.layer.borderWidth = 2.0
        }
    }
    @IBOutlet var todaysHoursLabel: UILabel?
    @IBOutlet var poolHoursLabel: UILabel?
    @IBOutlet var kidsClubHouseHours: UILabel?
    
    @IBOutlet var footerLogoImageView: UIImageView? {
        didSet {
            let plist = (UIApplication.shared.delegate as! AppDelegate).plist
            let imageName = plist?.object(forKey: "Navigation Bar Image") as! String
            let image = UIImage(named: imageName)
            footerLogoImageView?.image = image
        }
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupCountingLabels()
        setupTiles(rows: 3)
        setupConnectionsTiles(rows: 2)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setup UI
    
    func setupNavigationBar() {
        let plist = (UIApplication.shared.delegate as! AppDelegate).plist
        let imageName = plist?.object(forKey: "Navigation Bar Image") as! String
        let image = UIImage(named: imageName)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.titleView = imageView
        navigationController?.navigationBar.barTintColor = UIColor(red: 39.0 / 255.0, green: 132.0 / 255.0, blue: 174.0 / 255.0, alpha: 1.0)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func setupCountingLabels() {
        checkInCountLabel?.text = "2"
        durationCountLabel?.text = "7"
        pointsCountLabel?.text = "6"
    }
    
    func setupTiles(rows: Int) {
        let tileWidth = self.view.frame.size.width / 2.0
        let tileHeight = tileWidth / 16.0 * 9.0
        
        tileNavigationViewHeightConstraint?.constant = CGFloat(rows) * tileHeight
        
        let layoutNeeded = self.view.frame.size.width == 320 ? false : true
        
        for (index, imageView) in tilesCollection!.enumerated() {
            if index >= rows * 2 {
                imageView.isHidden = true
            } else {
                imageView.isHidden = false
            }
            
            if !layoutNeeded {
                continue
            }
            
            let constraints = imageView.constraints
            for constraint in constraints {
                if constraint.firstAttribute == .width {
                    constraint.constant = tileWidth
                } else if constraint.firstAttribute == .height {
                    constraint.constant = tileHeight
                }
            }
        }
        
        for (index, button) in tilesButtonCollection!.enumerated() {
            button.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            button.titleEdgeInsets.left = 10.0
            button.titleEdgeInsets.bottom = 5.0
            button.layer.borderColor = UIColor.white.cgColor
            button.layer.borderWidth = 1.0
            
            if index >= rows * 2 {
                button.isHidden = true
            } else {
                button.isHidden = false
            }
            
            if !layoutNeeded {
                continue
            }
            
            let constraints = button.constraints
            for constraint in constraints {
                if constraint.firstAttribute == .width {
                    constraint.constant = tileWidth
                } else if constraint.firstAttribute == .height {
                    constraint.constant = tileHeight
                }
            }
        }
        
        self.view.layoutIfNeeded()
    }
    
    func setupConnectionsTiles(rows: Int) {
        connectionViewHeightConstraint?.constant = 50 + 125 * CGFloat(rows)
        
        self.view.layoutIfNeeded()
        
        for (index, imageView) in connectionImageViewCollection!.enumerated() {
            if index >= rows {
                imageView.isHidden = true
            } else {
                imageView.isHidden = false
            }
        }
        
        for (index, button) in connectionButtonsCollection!.enumerated() {
            button.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            button.titleEdgeInsets.left = 60
            button.titleEdgeInsets.bottom = 22
            
            if index >= rows {
                button.isHidden = true
            } else {
                button.isHidden = false
            }
        }
        
        for (index, imageView) in connectionIconsCollection!.enumerated() {
            if index >= rows * 2 {
                imageView.isHidden = true
            } else {
                imageView.isHidden = false
            }
        }
        
        self.view.layoutIfNeeded()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
