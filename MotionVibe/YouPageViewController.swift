//
//  YouPageViewController.swift
//  MotionVibe
//
//  Created by Vadim Rupets on 11.10.16.
//  Copyright © 2016 Vadim Rupets. All rights reserved.
//

import UIKit

class YouPageViewController: UIViewController {
    
    //MARK: - Header UI elements
    
    @IBOutlet weak var avatarImageView: UIImageView? {
        didSet {
            avatarImageView?.layer.cornerRadius = 75.0 / 2.0
            avatarImageView?.layer.borderWidth = 2.0
            avatarImageView?.layer.borderColor = UIColor.white.cgColor
            avatarImageView?.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var favoritesCountLabel: UILabel? {
        didSet {
            favoritesCountLabel?.text = "3"
        }
    }
    
    @IBOutlet weak var badgesCountLabel: UILabel? {
        didSet {
            badgesCountLabel?.text = "2"
        }
    }
    
    @IBOutlet weak var connectionsLabel: UILabel? {
        didSet {
            connectionsLabel?.text = "19"
        }
    }
    
    //MARK: - WebView Buttons
    
    @IBOutlet weak var activityTimelineButton: UIButton? {
        didSet {
            activityTimelineButton?.isSelected = true
            activityTimelineButton?.backgroundColor = UIColor.init(red: 28.0 / 255.0, green: 82.0 / 255.0, blue: 105.0 / 255.0, alpha: 1)
            activityTimelineButton?.setTitleColor(UIColor.white, for: .selected)
        }
    }
    @IBOutlet weak var yourAccountButton: UIButton? {
        didSet {
            yourAccountButton?.setTitleColor(UIColor.white, for: .selected)
        }
    }
    
    //MARK: - WebView
    
    @IBOutlet weak var webView: UIWebView? {
        didSet {
            let webViewURL = URL.init(string: "https://instagram.com")
            let webViewRequest = URLRequest.init(url: webViewURL!)
            webView?.loadRequest(webViewRequest)
        }
    }

    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Setup UI
    
    func setupNavigationBar() {
        self.title = "John Testerson".uppercased()
        
        let attrs = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont(name: "Oswald-Regular", size: 20)!
        ]
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.barTintColor = UIColor(red: 39.0 / 255.0, green: 132.0 / 255.0, blue: 174.0 / 255.0, alpha: 1.0)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = attrs
        
        let backButtonImageView = UIImageView.init(frame: CGRect(x: 60, y: 9, width: 24, height: 24))
        backButtonImageView.image = #imageLiteral(resourceName: "Right Arrow")
        backButtonImageView.contentMode = .scaleAspectFit
        
        let backButton = UIButton.init(frame: CGRect(x: 0, y: 0, width: 75, height: 44))
        backButton.setTitleColor(UIColor.white, for: .normal)
        backButton.setTitle("Home", for: .normal)
        backButton.titleEdgeInsets.bottom = 2
        backButton.titleLabel?.textAlignment = .left
        backButton.addSubview(backButtonImageView)
        backButton.addTarget(self, action: #selector(didPressBackButton), for: .touchUpInside)
        
        let backButtonBarItem = UIBarButtonItem.init(customView: backButton)
        navigationItem.rightBarButtonItem = backButtonBarItem
        
        let settingsImageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        settingsImageView.image = #imageLiteral(resourceName: "Settings")
        settingsImageView.contentMode = .scaleAspectFit
        
        let settingsButton = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        settingsButton.addSubview(settingsImageView)
        settingsButton.addTarget(self, action: #selector(didPressSettingsButton), for: .touchUpInside)
        
        let settingsButtonBarItem = UIBarButtonItem.init(customView: settingsButton)
        navigationItem.leftBarButtonItem = settingsButtonBarItem
    }
    
    //MARK: - Buttons Actions

    @IBAction func didPressActivityTimelineButton() {
        if !activityTimelineButton!.isSelected {
            deselect(button: yourAccountButton!, selectButton: activityTimelineButton!)
            let webViewURL = URL.init(string: "https://instagram.com")
            let webViewRequest = URLRequest.init(url: webViewURL!)
            webView?.loadRequest(webViewRequest)
        }
    }
    
    @IBAction func didPressYourAccountButton() {
        if !yourAccountButton!.isSelected {
            deselect(button: activityTimelineButton!, selectButton: yourAccountButton!)
            let webViewURL = URL.init(string: "https://apple.com")
            let webViewRequest = URLRequest.init(url: webViewURL!)
            webView?.loadRequest(webViewRequest)
        }
    }
    
    func deselect(button: UIButton, selectButton: UIButton) {
        selectButton.isSelected = true
        selectButton.backgroundColor = UIColor.init(red: 28.0 / 255.0, green: 82.0 / 255.0, blue: 105.0 / 255.0, alpha: 1)
            
        button.isSelected = false
        button.backgroundColor = UIColor.init(red: 33.0 / 255.0, green: 103.0 / 255.0, blue: 134.0 / 255.0, alpha: 1)
    }
    
    func didPressBackButton() {
        _ = navigationController!.popViewController(animated: true)
    }
    
    func didPressSettingsButton() {
        let alertViewController = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let logoutAction = UIAlertAction(title: "Logout", style: .cancel) { [unowned self] (action) in
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
        alertViewController.addAction(logoutAction)
        
        let appSettingsAction = UIAlertAction(title: "App Settings", style: .default) { (action) in
            // add action for App Settings
        }
        alertViewController.addAction(appSettingsAction)
        
        let aboutAppAction = UIAlertAction(title: "About App", style: .default) { (action) in
            // add action for About App
        }
        alertViewController.addAction(aboutAppAction)
        
        self.present(alertViewController, animated: true) { 
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
